#include "Entity.h"

void Entity::Update(Level* level, unsigned int lastTime, unsigned int currentTime)
{

}

void Entity::Draw(Level* level)
{

}



Entity::Entity(float positionX, float positionY, float sizeX, float sizeY, int floorLevel, bool isCollidable)
{
	// Executed when class is created.
	Position.x = positionX;
	Position.y = positionY;
	Size.x = sizeX;
	Size.y = sizeY;
	FloorLevel = floorLevel;
	IsCollidable = isCollidable;
}


Entity::~Entity()
{
}
