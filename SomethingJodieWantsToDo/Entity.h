// A comment with chnages

#pragma once

#ifndef ENTITY_H
#define ENTITY_H

#include "Structs.h"

//#include "Level.h"
class Level;

class Entity
{
public:
	Vector2 Position;
	Vector2 Size;
	int FloorLevel;
	bool IsCollidable;

private:


protected:


public:
	virtual void Update(Level* level, unsigned int lastTime, unsigned int currentTime);
	virtual void Draw(Level* level);

	Entity(float positionX, float positionY, float sizeX, float sizeY, int floorLevel, bool isCollidable);
	~Entity();
};

#endif