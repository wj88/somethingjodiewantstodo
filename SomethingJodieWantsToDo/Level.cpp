#include <SDL.h>
#include <SDL_opengl.h>
#include <gl/GLU.h>

#include "Level.h"
#include "Platform.h"
#include "Wall.h"
#include "Soldier.h"

#define LoadToken(tokenVar)						\
tokenVar = strtok(NULL, ",");					\
if (tokenVar != NULL)							

#define LoadFloatToken(tokenVar, targetVar)		\
LoadToken(tokenVar)								\
	targetVar = atof(pch)

#define LoadIntToken(tokenVar, targetVar)		\
LoadToken(tokenVar)								\
	targetVar = atof(pch)

#define LoadStringToken(tokenVar, targetVar)	\
LoadToken(tokenVar)								\
	targetVar = pch



//GLuint boxList;

bool Level::Load()
{
	// Just incase we've already loaded, unload, then reload.
	Unload();

	// Define the filename
	char filename[20];
	sprintf(filename, "Level%d.csv", LevelNum);
	
	// Open the file
	FILE* f = fopen(filename, "r");
	if (f == NULL)
		return false;

	char buffer[1000];

	while (fgets(buffer, 1000, f) != NULL)
	{
		int length = strlen(buffer) - 1;

		// Trim the new line character
		buffer[length] = '\0';

		// Trim off comments
		char* comment = strchr(buffer, '#');
		if (comment != NULL)
			comment[0] = '\0';

		// Recalculate the length
		length = strlen(buffer);

		// Ignore lines without characters
		if (length > 0)
		{

			char* pch = strtok(buffer, ",");
			if (pch != NULL)
			{

				if (strcmp(pch, "platform") == 0)
				{
					float posX;
					float posY;
					float sizeX;
					float sizeY;
					int floorLevel;
					char* texture;
					unsigned int textureID;

					LoadFloatToken(pch, posX);
					LoadFloatToken(pch, posY);
					LoadFloatToken(pch, sizeX);
					LoadFloatToken(pch, sizeY);
					LoadIntToken(pch, floorLevel);
					LoadStringToken(pch, texture);
					
					textureID = GetTexture(texture);

					Platform* p = new Platform(posX, posY, false, sizeX, sizeY, floorLevel, textureID);
					Entities.push_back(p);
				}

				if (strcmp(pch, "wall") == 0)
				{
					float posX;
					float posY;
					float sizeX;
					float sizeY;
					int floorLevel;
					char* texture;
					unsigned int textureID;

					LoadFloatToken(pch, posX);
					LoadFloatToken(pch, posY);
					LoadFloatToken(pch, sizeX);
					LoadFloatToken(pch, sizeY);
					LoadIntToken(pch, floorLevel);
					LoadStringToken(pch, texture);

					textureID = GetTexture(texture);

					Wall* w = new Wall(posX, posY, true, sizeX, sizeY, floorLevel, textureID);
					Entities.push_back(w);
				}

				if (strcmp(pch, "soldier") == 0)
				{
					float posX;
					float posY;
					float sizeX;
					float sizeY;
					int floorLevel;
					char* texture;
					unsigned int textureID;

					LoadFloatToken(pch, posX);
					LoadFloatToken(pch, posY);
					LoadFloatToken(pch, sizeX);
					LoadFloatToken(pch, sizeY);
					LoadIntToken(pch, floorLevel);
					LoadStringToken(pch, texture);

					textureID = GetTexture(texture);

					Soldier* w = new Soldier(posX, posY, true, sizeX, sizeY, floorLevel, textureID);
					Entities.push_back(w);
				}
			}

		}
	}

	// Close the file
	fclose(f);


	//boxList = glGenLists(1);
	//glNewList(boxList, GL_COMPILE);



	//glEndList();


	return true;
}

unsigned int Level::GetTexture(char* filename)
{
	// Are there any textures in our map, matching the given key?
	if (Textures.count(filename) == 0)
	{
		// If not, load...
		
		unsigned char header[54]; // Somewhere to store the bitmap data
		unsigned int dataPos; // Location in file where the actual data starts
		unsigned int width, height; // Dimensions of the bitmap
		unsigned int imageSize; // =width*height*3
		unsigned char* data;

		FILE* file = fopen(filename, "rb");
		if (!file)
		{
			printf("Image could not be opened: %s\n", filename);
			return 0;
		}

		if (fread(header, 1, 54, file) != 54)
		{
			printf("No a correct BMP file: %s\n", filename);
			fclose(file);
			return 0;
		}

		if (header[0] != 'B' || header[1] != 'M')
		{
			printf("No a correct BMP file: %s\n", filename);
			fclose(file);
			return 0;
		}

		dataPos   = *((int*)&header[0x0A]);
		imageSize = *((int*)&header[0x22]);
		width =		*((int*)&header[0x12]);
		height =	*((int*)&header[0x16]);

		if (imageSize == 0) imageSize = width*height * 3;
		if (dataPos == 0) dataPos = 54;

		data = new unsigned char[imageSize];

		// Read the data from the file into our buffer
		fread(data, 1, imageSize, file);

		fclose(file);

		unsigned int textureID;
		glGenTextures(1, &textureID);
		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, data);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		delete data;

		char* key = new char[strlen(filename) + 1];
		strcpy(key, filename);
		Textures[key] = textureID;
		TextureSizes[textureID].x = width;
		TextureSizes[textureID].y = height;
	}

	return Textures[filename];
}

void Level::Unload()
{
	//glDeleteLists(boxList, 1);

	TextureSizes.clear();
	for (map<char*, unsigned int>::iterator iter = Textures.begin(); iter != Textures.end(); ++iter)
	{
		glDeleteTextures(1, &iter->second);
		delete iter->first;
	}
	Textures.clear();

	for (int i = 0; i < Entities.size(); i++)
	{
		delete Entities.at(i);
	}
	Entities.clear();
}

void Level::Update(unsigned int lastTime, unsigned int currentTime)
{
	float moveAmount = ((float)currentTime - (float)lastTime);

	const Uint8* state = SDL_GetKeyboardState(NULL);

	Vector2 targetPos;
	targetPos.x = PlayerPosition.x;
	targetPos.y = PlayerPosition.y;

	if (state[SDL_SCANCODE_A])
		PlayerRotation += ((2 / PI) / moveAmount);
	if (state[SDL_SCANCODE_D])
		PlayerRotation -= ((2 / PI) / moveAmount);
	if (state[SDL_SCANCODE_W])
	{
		targetPos.x += ((3.0f / moveAmount) * sinf(PlayerRotation));
		targetPos.y += ((3.0f / moveAmount) * cosf(PlayerRotation));
	}
	if (state[SDL_SCANCODE_S])
	{
		targetPos.x -= ((3.0f / moveAmount) * sinf(PlayerRotation));
		targetPos.y -= ((3.0f / moveAmount) * cosf(PlayerRotation));
	}




	// Do some collision checking.
	float ourRadius = 0.6f;
	bool isColliding = false;
	for (int i = 0; i < Entities.size(); i++)
	{
		Entity* e = Entities[i];
		if (e->IsCollidable && e->FloorLevel == 1)
		{
			Vector2 theirPos, theirSize, ourPos, ourSize;

			theirPos.x = e->Position.x;
			theirPos.y = e->Position.y;
			theirSize.x = e->Size.x;
			theirSize.y = e->Size.y;

			ourPos.x = targetPos.x - 0.6f;
			ourPos.y = targetPos.y - 0.6f;
			ourSize.x = 1.2f;
			ourSize.y = 1.2f;

			// We are colliding on the x and y axis
			if ((ourPos.x < theirPos.x + theirSize.x && ourPos.x + ourSize.x > theirPos.x) && (ourPos.y < theirPos.y + theirSize.y && ourPos.y + ourSize.y > theirPos.y))
			{
				isColliding = true;
			}
		}
	}



	if (!isColliding)
	{
		PlayerPosition.x = targetPos.x;
		PlayerPosition.y = targetPos.y;
	}


	for (int i = 0; i < Entities.size(); i++)
	{
		Entities.at(i)->Update(this, lastTime, currentTime);
	}
}

void Level::Draw()
{
	for (int i = 0; i < Entities.size(); i++)
	{
		Entities.at(i)->Draw(this);
	}
}

void Level::DrawBox(Vector3 pos, Vector3 scale, Vector3 rotation, unsigned int textureID)
{
	glBindTexture(GL_TEXTURE_2D, textureID);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	Vector2 lookAt;
	lookAt.x = PlayerPosition.x + sinf(PlayerRotation);
	lookAt.y = PlayerPosition.y + cosf(PlayerRotation);

	// The camera is location -10.0 on the Z axis, looking at the centre of the world, and +1 on the Y axis is up.
	//		 (Position		                          | Look at                | Up		   )
	gluLookAt(PlayerPosition.x, 1.5, PlayerPosition.y, lookAt.x, 1.5, lookAt.y, 0.0, 1.0, 0.0);


	glTranslatef(pos.x, pos.y, pos.z);
	glScalef(scale.x, scale.y, scale.z);
	glRotatef(rotation.x, 1.0, 0.0, 0.0);
	glRotatef(rotation.y, 0.0, 1.0, 0.0);
	glRotatef(rotation.z, 0.0, 0.0, 1.0);

	//glCallList(boxList);

	// Vertices of a cube which is 1x1x1
	//					X,		Y,		Z
	GLfloat v0[] = { -0.5f,	-0.5f,	 0.5f };
	GLfloat v1[] = { 0.5f,	-0.5f,	 0.5f };
	GLfloat v2[] = { 0.5f,	 0.5f,	 0.5f };
	GLfloat v3[] = { -0.5f,	 0.5f,	 0.5f };
	GLfloat v4[] = { -0.5f,	-0.5f,	-0.5f };
	GLfloat v5[] = { 0.5f,	-0.5f,	-0.5f };
	GLfloat v6[] = { 0.5f,	 0.5f,	-0.5f };
	GLfloat v7[] = { -0.5f,	 0.5f,	-0.5f };

	// Front/Back
	GLfloat fb_t0[] = { 0.0f,		0.0f		};
	GLfloat fb_t1[] = { scale.x,	0.0f		};
	GLfloat fb_t2[] = { scale.x,	scale.y		};
	GLfloat fb_t3[] = { 0.0f,		scale.y		};

	// Left/Right
	GLfloat lr_t0[] = { 0.0f,		0.0f		};
	GLfloat lr_t1[] = { scale.z,	0.0f		};
	GLfloat lr_t2[] = { scale.z,	scale.y		};
	GLfloat lr_t3[] = { 0.0f,		scale.y		};

	// Top/Bottom
	GLfloat tb_t0[] = { 0.0f,		0.0f		};
	GLfloat tb_t1[] = { scale.x,	0.0f		};
	GLfloat tb_t2[] = { scale.x,	scale.z		};
	GLfloat tb_t3[] = { 0.0f,		scale.z		};

	// Tell OpenGL, and in turn the graphics card, we about to draw triangles.
	glBegin(GL_TRIANGLES);

	// Front

	// 1
	glTexCoord2fv(fb_t0);
	glVertex3fv(v0);
	glTexCoord2fv(fb_t1);
	glVertex3fv(v1);
	glTexCoord2fv(fb_t2);
	glVertex3fv(v2);

	// 2
	glTexCoord2fv(fb_t0);
	glVertex3fv(v0);
	glTexCoord2fv(fb_t2);
	glVertex3fv(v2);
	glTexCoord2fv(fb_t3);
	glVertex3fv(v3);

	// Right side

	// 3
	glTexCoord2fv(lr_t0);
	glVertex3fv(v1);
	glTexCoord2fv(lr_t1);
	glVertex3fv(v5);
	glTexCoord2fv(lr_t2);
	glVertex3fv(v6);

	// 4
	glTexCoord2fv(lr_t0);
	glVertex3fv(v1);
	glTexCoord2fv(lr_t2);
	glVertex3fv(v6);
	glTexCoord2fv(lr_t3);
	glVertex3fv(v2);

	// Back

	// 5
	glTexCoord2fv(fb_t0);
	glVertex3fv(v5);
	glTexCoord2fv(fb_t1);
	glVertex3fv(v4);
	glTexCoord2fv(fb_t2);
	glVertex3fv(v7);

	// 6
	glTexCoord2fv(fb_t0);
	glVertex3fv(v5);
	glTexCoord2fv(fb_t2);
	glVertex3fv(v7);
	glTexCoord2fv(fb_t3);
	glVertex3fv(v6);

	// Left side

	// 7
	glTexCoord2fv(lr_t0);
	glVertex3fv(v4);
	glTexCoord2fv(lr_t1);
	glVertex3fv(v0);
	glTexCoord2fv(lr_t2);
	glVertex3fv(v3);

	// 8
	glTexCoord2fv(lr_t0);
	glVertex3fv(v4);
	glTexCoord2fv(lr_t2);
	glVertex3fv(v3);
	glTexCoord2fv(lr_t3);
	glVertex3fv(v7);

	// Top

	// 9
	glTexCoord2fv(tb_t0);
	glVertex3fv(v3);
	glTexCoord2fv(tb_t1);
	glVertex3fv(v2);
	glTexCoord2fv(tb_t2);
	glVertex3fv(v6);

	// 10
	glTexCoord2fv(tb_t0);
	glVertex3fv(v3);
	glTexCoord2fv(tb_t2);
	glVertex3fv(v6);
	glTexCoord2fv(tb_t3);
	glVertex3fv(v7);

	// Bottom

	// 11
	glTexCoord2fv(tb_t0);
	glVertex3fv(v1);
	glTexCoord2fv(tb_t1);
	glVertex3fv(v0);
	glTexCoord2fv(tb_t2);
	glVertex3fv(v4);

	// 12
	glTexCoord2fv(tb_t0);
	glVertex3fv(v1);
	glTexCoord2fv(tb_t2);
	glVertex3fv(v4);
	glTexCoord2fv(tb_t3);
	glVertex3fv(v5);

	glEnd();
}

void Level::DrawSprite(Vector3 pos, Vector2 size, Vector2 spriteSize, Vector2 spriteIndex, unsigned int textureID)
{
	Vector2 textureSize = TextureSizes[textureID];
	Vector2 scaledTextureSize;
	scaledTextureSize.x = (1.0f / textureSize.x);
	scaledTextureSize.y = (1.0f / textureSize.y);

	Vector2 scaledSprite;
	scaledSprite.x = scaledTextureSize.x * spriteSize.x;
	scaledSprite.y = scaledTextureSize.y * spriteSize.y;



	glBindTexture(GL_TEXTURE_2D, textureID);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();


	Vector2 lookAt;
	lookAt.x = PlayerPosition.x + sinf(PlayerRotation);
	lookAt.y = PlayerPosition.y + cosf(PlayerRotation);

	// The camera is location -10.0 on the Z axis, looking at the centre of the world, and +1 on the Y axis is up.
	//		 (Position		                          | Look at                | Up		   )
	gluLookAt(PlayerPosition.x, 1.5, PlayerPosition.y, lookAt.x, 1.5, lookAt.y, 0.0, 1.0, 0.0);


	float x = pos.x - PlayerPosition.x;
	float y = pos.z - PlayerPosition.y;
	float a = -atan2(y, x) - (PI / 2);
	a =  a * 180.0f / PI;				// Degrees
	printf("Rot: %f\n", a);


	glTranslatef(pos.x, pos.y, pos.z);
	glRotatef(a, 0.0, 1.0f, 0.0);		// Degrees
	glScalef(size.x, size.y, 1.0f);

	//glCallList(boxList);

	// Vertices of a cube which is 1x1x1
	//					X,		Y,		Z
	GLfloat v0[] = { -0.5f,	-0.5f,	 0.5f };
	GLfloat v1[] = { 0.5f,	-0.5f,	 0.5f };
	GLfloat v2[] = { 0.5f,	 0.5f,	 0.5f };
	GLfloat v3[] = { -0.5f,	 0.5f,	 0.5f };

	// Front/Back
	GLfloat fb_t0[] = { (scaledSprite.x * spriteIndex.x) + 0.0f,				(scaledSprite.y * spriteIndex.y) + 0.0f };
	GLfloat fb_t1[] = { (scaledSprite.x * spriteIndex.x) + scaledSprite.x,		(scaledSprite.y * spriteIndex.y) + 0.0f };
	GLfloat fb_t2[] = { (scaledSprite.x * spriteIndex.x) + scaledSprite.x,		(scaledSprite.y * spriteIndex.y) + scaledSprite.y };
	GLfloat fb_t3[] = { (scaledSprite.x * spriteIndex.x) + 0.0f,				(scaledSprite.y * spriteIndex.y) + scaledSprite.y };

	// Tell OpenGL, and in turn the graphics card, we about to draw triangles.
	glBegin(GL_TRIANGLES);

	// Front

	// 1
	glTexCoord2fv(fb_t0);
	glVertex3fv(v0);
	glTexCoord2fv(fb_t1);
	glVertex3fv(v1);
	glTexCoord2fv(fb_t2);
	glVertex3fv(v2);

	// 2
	glTexCoord2fv(fb_t0);
	glVertex3fv(v0);
	glTexCoord2fv(fb_t2);
	glVertex3fv(v2);
	glTexCoord2fv(fb_t3);
	glVertex3fv(v3);

	glEnd();
}

Level::Level(int levelNum)
{
	LevelNum = levelNum;
	PlayerPosition.x = 0.0f;
	PlayerPosition.y = 0.0f;
	PlayerRotation = 0.0f;
}

Level::~Level()
{
	Unload();
}
