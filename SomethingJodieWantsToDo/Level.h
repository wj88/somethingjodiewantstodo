#pragma once

#ifndef LEVEL_H
#define LEVEL_H

#include <vector>
#include <map>
using namespace std;

#include "Entity.h"

#define PI 3.1415f
#define FLOOR_HEIGHT 2.5f

struct str_cmp
{
	bool operator()(char const *a, char const *b)
	{
		return std::strcmp(a, b) < 0;
	}
};

class Level
{
public:
	vector<Entity*> Entities;
	map<char*, unsigned int, str_cmp> Textures;
	map<unsigned int, Vector2> TextureSizes;
	int LevelNum;
	Vector2 PlayerPosition;
	float PlayerRotation;

public:
	bool Load();
	unsigned int GetTexture(char* filename);
	void Unload();

public:
	void Update(unsigned int lastTime, unsigned int currentTime);
	void Draw();
	void DrawBox(Vector3 pos, Vector3 scale, Vector3 rotation, unsigned int textureID);
	void DrawSprite(Vector3 pos, Vector2 size, Vector2 spriteSize, Vector2 spriteIndex, unsigned int textureID);

	Level(int levelNum);
	~Level();
};

#endif