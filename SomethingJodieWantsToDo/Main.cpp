#include <stdio.h>
#include <SDL.h>
#include <SDL_opengl.h>
#include <gl/GLU.h>
#include "Level.h"


#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600

SDL_Window* win = NULL;
SDL_GLContext glcontext;

Level* level;

//GLuint cubeList;

//// Vertices of a cube which is 2x2x2
////					X,		Y,		Z
//GLfloat v0[] =	{	-1.0f,	-1.0f,	 1.0f	};
//GLfloat v1[] =	{	 1.0f,	-1.0f,	 1.0f	};
//GLfloat v2[] =	{	 1.0f,	 1.0f,	 1.0f	};
//GLfloat v3[] =	{	-1.0f,	 1.0f,	 1.0f	};
//GLfloat v4[] =	{	-1.0f,	-1.0f,	-1.0f	};
//GLfloat v5[] =	{	 1.0f,	-1.0f,	-1.0f	};
//GLfloat v6[] =	{	 1.0f,	 1.0f,	-1.0f	};
//GLfloat v7[] =	{	-1.0f,	 1.0f,	-1.0f	};
//
//// Colours mapped to the vertices
////						R,		G,		B,		A
//GLubyte red[]		= {	255,	0,		0,		255		};
//GLubyte green[]		= { 0,		255,	0,		255		};
//GLubyte blue[]		= { 0,		0,		255,	255		};
//GLubyte white[] 	= { 255,	255,	255,	255		};
//GLubyte yellow[]	= { 255,	255,	0,		255		};
//GLubyte black[]		= { 0,		0,		0,		255		};
//GLubyte turquoise[]	= { 0,		255,	255,	255		};
//GLubyte purple[]	= { 102,	0,		204,	255		};

//					X	 Y	  Z
GLfloat	angle[] = { 0.0, 0.0, 0.0 };
GLfloat	pos[]   = { 0.0, 0.0, 0.0 };
GLfloat scale = 1.0f;

// Init will initialise SDL and create a window. If this succeeds it returns true. If it fails, it returns false.
bool Init()
{
	if (SDL_Init(SDL_INIT_EVERYTHING) == 0)
	{
		// SDL is initialised!
		win = SDL_CreateWindow("Hello World!", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
		if (win != NULL)
		{
			// Window is created!

			// Turn on double buffering - 1 = enable, 0 = disable
			SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

			// Create an OpenGL context for doing 3D stuff
			glcontext = SDL_GL_CreateContext(win);

			// Enable Texture Mapping
			glEnable(GL_TEXTURE_2D);

			// Enable the depth buffer
			glEnable(GL_DEPTH_TEST);

			// Setup the OpenGL shader!
			glShadeModel(GL_SMOOTH);

			// Setup culling options. Draw only the front, clock-wise.
			glCullFace(GL_FRONT);
			glFrontFace(GL_CW);
			glEnable(GL_CULL_FACE);

			// Set the clear colour
			glClearColor(0, 0, 0, 1);

			// Setup the viewport
			glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

			// Projection = what we're setting up.
			// Perspective = is what want.
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			gluPerspective(90.0, (GLdouble)WINDOW_WIDTH / (GLdouble)WINDOW_HEIGHT, 0.01, 1000.0);

			return true;
		}
		SDL_Quit();
	}
	return false;
}

void DeInit()
{
	SDL_GL_DeleteContext(glcontext);

	// If we have a window, destroy it.
	if(win != NULL)
		SDL_DestroyWindow(win);

	// Quit SDL.
	SDL_Quit();
}

bool LoadContent()
{
	level = new Level(1);
	if (level->Load() == false)
	{
		delete level;
		return false;
	}

	//cubeList = glGenLists(1);
	//glNewList(cubeList, GL_COMPILE);

	//// Tell OpenGL, and in turn the graphics card, we about to draw triangles.
	//glBegin(GL_TRIANGLES);

	//// 1
	//glColor4ubv(red);
	//glVertex3fv(v0);
	//glColor4ubv(green);
	//glVertex3fv(v1);
	//glColor4ubv(blue);
	//glVertex3fv(v2);

	//// 2
	//glColor4ubv(red);
	//glVertex3fv(v0);
	//glColor4ubv(blue);
	//glVertex3fv(v2);
	//glColor4ubv(white);
	//glVertex3fv(v3);

	//// 3
	//glColor4ubv(green);
	//glVertex3fv(v1);
	//glColor4ubv(black);
	//glVertex3fv(v5);
	//glColor4ubv(turquoise);
	//glVertex3fv(v6);

	//// 4
	//glColor4ubv(green);
	//glVertex3fv(v1);
	//glColor4ubv(turquoise);
	//glVertex3fv(v6);
	//glColor4ubv(blue);
	//glVertex3fv(v2);

	//// 5
	//glColor4ubv(black);
	//glVertex3fv(v5);
	//glColor4ubv(yellow);
	//glVertex3fv(v4);
	//glColor4ubv(purple);
	//glVertex3fv(v7);

	//// 6
	//glColor4ubv(black);
	//glVertex3fv(v5);
	//glColor4ubv(purple);
	//glVertex3fv(v7);
	//glColor4ubv(turquoise);
	//glVertex3fv(v6);

	//// 7
	//glColor4ubv(yellow);
	//glVertex3fv(v4);
	//glColor4ubv(red);
	//glVertex3fv(v0);
	//glColor4ubv(white);
	//glVertex3fv(v3);

	//// 8
	//glColor4ubv(yellow);
	//glVertex3fv(v4);
	//glColor4ubv(white);
	//glVertex3fv(v3);
	//glColor4ubv(purple);
	//glVertex3fv(v7);

	//// 9
	//glColor4ubv(white);
	//glVertex3fv(v3);
	//glColor4ubv(blue);
	//glVertex3fv(v2);
	//glColor4ubv(turquoise);
	//glVertex3fv(v6);

	//// 10
	//glColor4ubv(white);
	//glVertex3fv(v3);
	//glColor4ubv(turquoise);
	//glVertex3fv(v6);
	//glColor4ubv(purple);
	//glVertex3fv(v7);

	//// 11
	//glColor4ubv(green);
	//glVertex3fv(v1);
	//glColor4ubv(red);
	//glVertex3fv(v0);
	//glColor4ubv(yellow);
	//glVertex3fv(v4);

	//// 12
	//glColor4ubv(green);
	//glVertex3fv(v1);
	//glColor4ubv(yellow);
	//glVertex3fv(v4);
	//glColor4ubv(black);
	//glVertex3fv(v5);

	//glEnd();

	//glEndList();

	return true;
}

void UnloadContent()
{
	//glDeleteLists(cubeList, 1);

	level->Unload();
	delete level;
}

bool Update(unsigned int lastTime, unsigned int currentTime)
{
	SDL_PumpEvents();

	//if (state[SDL_SCANCODE_A])
	//	angle[1] -= moveAmount;
	//if (state[SDL_SCANCODE_D])
	//	angle[1] += moveAmount;
	//if (state[SDL_SCANCODE_W])
	//	angle[0] -= moveAmount;
	//if (state[SDL_SCANCODE_S])
	//	angle[0] += moveAmount;
	//if (state[SDL_SCANCODE_Q])
	//	angle[2] -= moveAmount;
	//if (state[SDL_SCANCODE_E])
	//	angle[2] += moveAmount;

	//if (state[SDL_SCANCODE_L])
	//	pos[0] -= moveAmount * 0.1f;
	//if (state[SDL_SCANCODE_J])
	//	pos[0] += moveAmount * 0.1f;
	//if (state[SDL_SCANCODE_K])
	//	pos[1] -= moveAmount * 0.1f;
	//if (state[SDL_SCANCODE_I])
	//	pos[1] += moveAmount * 0.1f;
	//if (state[SDL_SCANCODE_O])
	//	pos[2] -= moveAmount * 0.1f;
	//if (state[SDL_SCANCODE_U])
	//	pos[2] += moveAmount * 0.1f;

	//if (state[SDL_SCANCODE_KP_PLUS])
	//	scale += moveAmount * 0.01f;
	//if (state[SDL_SCANCODE_KP_MINUS])
	//	scale -= moveAmount * 0.01f;
	
	// If you're pressing either left or right Alt, and F4, return false.
	const Uint8* state = SDL_GetKeyboardState(NULL);
	if ((state[SDL_SCANCODE_LALT] || state[SDL_SCANCODE_RALT]) && state[SDL_SCANCODE_F4])
		return false;

	level->Update(lastTime, currentTime);

	return true;
}

//void DrawCube()
//{
//	glCallList(cubeList);
//}

unsigned int lastSec = 0;
unsigned int fps = 0;

void Draw(unsigned int lastTime, unsigned int currentTime)
{
	// Must do clear first.
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	// Model View Matrix

	// Camera combined with the model transform
	//glMatrixMode(GL_MODELVIEW);
	//glLoadIdentity();

	// The camera is location -10.0 on the Z axis, looking at the centre of the world, and +1 on the Y axis is up.
	// gluLookAt(0.0, 0.0, -5.0, 0, 0, 0, 0.0, 1.0, 0.0);

	level->Draw();

	//glScalef(scale, scale, scale);
	//glTranslatef(pos[0], pos[1], pos[2]);
	//glRotatef(angle[0], 1.0, 0.0, 0.0);
	//glRotatef(angle[1], 0.0, 1.0, 0.0);
	//glRotatef(angle[2], 0.0, 0.0, 1.0);

	//DrawCube();

	//glMatrixMode(GL_MODELVIEW);
	//glLoadIdentity();
	//gluLookAt(0.0, 0.0, -5.0, 0, 0, 0, 0.0, 1.0, 0.0);
	//glTranslatef(5.0f, 0.0f, 0.0f);
	//glRotatef(angle[0], 1.0, 0.0, 0.0);
	//glRotatef(angle[1], 0.0, 1.0, 0.0);
	//glRotatef(angle[2], 0.0, 0.0, 1.0);
	//DrawCube();

	//glMatrixMode(GL_MODELVIEW);
	//glLoadIdentity();
	//gluLookAt(0.0, 0.0, -5.0, 0, 0, 0, 0.0, 1.0, 0.0);
	//glTranslatef(-5.0f, 0.0f, 0.0f);
	//glRotatef(angle[0], 1.0, 0.0, 0.0);
	//glRotatef(angle[1], 0.0, 1.0, 0.0);
	//glRotatef(angle[2], 0.0, 0.0, 1.0);
	//DrawCube();


	// Present the changes to the backbuffer to the window
	SDL_GL_SwapWindow(win);

	// Time in seconds since sdl init.
	fps++;
	unsigned int secs = (unsigned int)(currentTime / 1000.0);
	if (secs != lastSec)
	{
		printf("At %d, fps: %d\n", secs, fps);
		fps = 0;
		lastSec = secs;
	}
}

bool HandleEvents()
{
	// Poll for events. SDL_PollEvent() returns 0 when there are no
	// more events on the event queue, our while loop will exit when
	// that occurs.
	bool keepGoing = true;
	SDL_Event event;
	while (keepGoing && SDL_PollEvent(&event) != 0)
	{
		// We are only worried about SDL_KEYDOWN and SDL_KEYUP events
		switch (event.type)
		{
			case SDL_KEYDOWN:
				printf("Key press detected\n");
				break;

			case SDL_KEYUP:
				printf("Key release detected\n");
				break;

			case SDL_QUIT:
				keepGoing = false;
				break;
		}
	}

	return keepGoing;
}

int main(int argc, char** argv)
{
	unsigned int targetFPS = 1000 / 60;

	if (!Init())
		return 1;

	if (!LoadContent())
	{
		DeInit();
		return 2;
	}

	unsigned int lastTime = SDL_GetTicks();

	while (HandleEvents())
	{
		unsigned int currentTime = SDL_GetTicks();


		if (!Update(lastTime, currentTime))
			break;

		Draw(lastTime, currentTime);

		// Try and stick to the target FPS.
		int sleepTime = targetFPS - (SDL_GetTicks() - currentTime);
		if (sleepTime > 0)
			SDL_Delay(sleepTime);

		lastTime = currentTime;
	}

	UnloadContent();

	DeInit();
			
	return 0;
}