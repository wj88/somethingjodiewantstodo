#include "Platform.h"

void Platform::Draw(Level* level)
{
	Vector3 pos;
	Vector3 rot;
	Vector3 scale;

	pos.x = Position.x + (Size.x / 2.0f);
	pos.y = FloorLevel * FLOOR_HEIGHT;
	pos.z = Position.y + (Size.y / 2.0f);

	rot.x = 0.0f;
	rot.y = 0.0f;
	rot.z = 0.0f;

	scale.x = Size.x;
	scale.y = 0.1f;
	scale.z = Size.y;

	level->DrawBox(pos, scale, rot, TextureID);
}


Platform::Platform(float positionX, float positionY, bool isCollidable, float sizeX, float sizeY, int floorLevel, unsigned int textureID) : Entity(positionX, positionY, sizeX, sizeY, floorLevel, isCollidable)
{
	TextureID = textureID;
}


Platform::~Platform()
{
}
