#pragma once

#ifndef PLATFORM_H
#define PLATFORM_H


#include "Entity.h"
#include "Level.h"

class Platform :
	public Entity
{
public:
	unsigned int TextureID;

public:
	virtual void Draw(Level* level);

	Platform(float positionX, float positionY, bool isCollidable, float sizeX, float sizeY, int floorLevel, unsigned int textureID);
	~Platform();
};

#endif