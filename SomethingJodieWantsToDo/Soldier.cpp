#include "Soldier.h"

void Soldier::Update(Level* level, unsigned int lastTime, unsigned int currentTime)
{
	if (currentTime - lastTimeChanged > 500)
	{
		if (SpriteIndex.x == 4 && SpriteIndex.y == 3)
		{
			SpriteIndex.x = 0;
			SpriteIndex.y = 2;
		}
		else if(SpriteIndex.x == 0 && SpriteIndex.y == 2)
		{
			SpriteIndex.x = 8;
			SpriteIndex.y = 2;
		}
		else
		{
			SpriteIndex.x = 4;
			SpriteIndex.y = 3;
		}

		lastTimeChanged = currentTime;
	}
}

Soldier::Soldier(float positionX, float positionY, bool isCollidable, float sizeX, float sizeY, int floorLevel, unsigned int textureID) : SpriteSheetEntity(positionX, positionY, isCollidable, sizeX, sizeY, 65, 65, floorLevel, textureID)
{
	lastTimeChanged = 0;
	SpriteIndex.x = 4;
	SpriteIndex.y = 3;
}


Soldier::~Soldier()
{
}
