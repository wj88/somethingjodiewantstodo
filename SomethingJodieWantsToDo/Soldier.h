#pragma once
#include "SpriteSheetEntity.h"

class Soldier :
	public SpriteSheetEntity
{
private:
	unsigned int lastTimeChanged;

public:
	virtual void Update(Level* level, unsigned int lastTime, unsigned int currentTime);

	Soldier(float positionX, float positionY, bool isCollidable, float sizeX, float sizeY, int floorLevel, unsigned int textureID);
	~Soldier();
};

