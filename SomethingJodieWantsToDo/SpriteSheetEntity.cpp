#include "SpriteSheetEntity.h"

void SpriteSheetEntity::Draw(Level* level)
{
	Vector3 pos;
	pos.x = Position.x + (Size.x / 2.0f);
	pos.y = FloorLevel * FLOOR_HEIGHT - (Size.y / 2);
	pos.z = Position.y + (Size.y / 2.0f);

	level->DrawSprite(pos, Size, SpriteSize, SpriteIndex, TextureID);
}


SpriteSheetEntity::SpriteSheetEntity(float positionX, float positionY, bool isCollidable, float sizeX, float sizeY, float spriteSizeWidth, float spriteSizeHeight, int floorLevel, unsigned int textureID) : Entity(positionX, positionY, sizeX, sizeY, floorLevel, isCollidable)
{
	SpriteSize.x = spriteSizeWidth;
	SpriteSize.y = spriteSizeHeight;

	SpriteIndex.x = 0;
	SpriteIndex.y = 0;
	TextureID = textureID;
}


SpriteSheetEntity::~SpriteSheetEntity()
{
}
