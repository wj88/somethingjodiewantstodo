#pragma once
#include "Entity.h"
#include "Level.h"

class SpriteSheetEntity :
	public Entity
{
public:
	Vector2 SpriteSize;
	Vector2 SpriteIndex;
	unsigned int TextureID;

public:
	virtual void Draw(Level* level);

	SpriteSheetEntity(float positionX, float positionY, bool isCollidable, float sizeX, float sizeY, float spriteSizeWidth, float spriteSizeHeight, int floorLevel, unsigned int textureID);
	~SpriteSheetEntity();
};

