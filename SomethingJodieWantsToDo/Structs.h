#pragma once

#ifndef STRUCTS_H
#define STRUCTS_H

struct Vector2
{
public:
	float x;
	float y;
};

struct Vector3
{
public:
	float x;
	float y;
	float z;
};

#endif