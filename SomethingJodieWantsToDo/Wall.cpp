#include "Wall.h"


void Wall::Draw(Level* level)
{
	Vector3 pos;
	Vector3 rot;
	Vector3 scale;

	pos.x = Position.x + (Size.x / 2.0f);
	pos.y = (FloorLevel * FLOOR_HEIGHT) - (FLOOR_HEIGHT / 2.0f);
	pos.z = Position.y + (Size.y / 2.0f);

	rot.x = 0.0f;
	rot.y = 0.0f;
	rot.z = 0.0f;

	scale.x = Size.x;
	scale.y = FLOOR_HEIGHT;
	scale.z = Size.y;

	level->DrawBox(pos, scale, rot, TextureID);
}


Wall::Wall(float positionX, float positionY, bool isCollidable, float sizeX, float sizeY, int floorLevel, unsigned int textureID) : Entity(positionX, positionY, sizeX, sizeY, floorLevel, isCollidable)
{
	TextureID = textureID;
}


Wall::~Wall()
{
}
