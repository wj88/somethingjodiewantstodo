#pragma once

#include "Entity.h"
#include "Level.h"

class Wall :
	public Entity
{
public:
	unsigned int TextureID;

public:
	virtual void Draw(Level* level);

	Wall(float positionX, float positionY, bool isCollidable, float sizeX, float sizeY, int floorLevel, unsigned int textureID);
	~Wall();
};

